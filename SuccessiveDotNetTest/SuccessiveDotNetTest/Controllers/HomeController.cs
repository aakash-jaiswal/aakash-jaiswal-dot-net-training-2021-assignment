﻿using SuccessiveDotNetTest.Database;
using SuccessiveDotNetTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;

namespace SuccessiveDotNetTest.Controllers
{
    public class HomeController : Controller
    {
        private TestEntities db = new TestEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        //For Dropdown
        public ActionResult ScheduleInterview()
        {
            var getResult = db.tbl_candidates.ToList();
            //var getResult = db.tbl_candidates.ToList();
            Merge merge = new Merge();
            List<CandidateModel> candidatelist = new List<CandidateModel>();

            candidatelist = (from c in db.tbl_candidates
                             join s in db.tbl_interviewschedules on c.Id equals s.Candidate_Id
                             orderby s.Date descending
                             select new CandidateModel
                             {
                                 Id = c.Id,
                                 First_Name = c.First_Name + " " + c.Last_Name + " (" + c.Email + ")",
                             }).ToList();
            if (candidatelist.Count > 0)
                merge.CanDdl = new SelectList(candidatelist.ToList(), "Id", "First_Name");
            else {
                merge.CanDdl = new SelectList(Enumerable.Empty<SelectListItem>());
            }
            return View(merge);
        }
        //Add Data Here
        [HttpPost]
        public ActionResult ScheduleInterview(Merge model)
        {
            if (model.candidateModel.Id == 0)
            {
                tbl_candidates candidates = new tbl_candidates();
                tbl_interviewschedules interviewschedules = new tbl_interviewschedules();



                candidates.First_Name = model.candidateModel.First_Name;
                candidates.Last_Name = model.candidateModel.Last_Name;

                candidates.Email = model.Email;
                candidates.Dob = model.candidateModel.Dob;
                candidates.Mobile = model.candidateModel.Mobile;
                candidates.Experience = model.candidateModel.Experience;


                interviewschedules.Date = model.interviewModel.Date;
                interviewschedules.Time_From = model.interviewModel.Time_From;
                interviewschedules.Time_To = model.interviewModel.Time_To;
                interviewschedules.Interviewer_Name = model.interviewModel.Interviewer_Name;

                db.tbl_candidates.Add(candidates);
                db.tbl_interviewschedules.Add(interviewschedules);
                db.SaveChanges();
                return RedirectToAction("ScheduleInterview");
            }
            //Editing records
            else {
                tbl_candidates candidates = db.tbl_candidates.FirstOrDefault(x => x.Id == model.candidateModel.Id);
                    candidates.Mobile = model.candidateModel.Mobile;
                candidates.Experience = model.candidateModel.Experience;
                db.Entry(candidates).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ScheduledInterviewDetails");
            }


                
           
        }
        //View Data Here
        public ActionResult ScheduledInterviewDetails()
        {
            Merge merge = new Merge();
            List<CandidateModel> candidatelist = new List<CandidateModel>();

            candidatelist = (from c in db.tbl_candidates
                             join s in db.tbl_interviewschedules on c.Id equals s.Candidate_Id
                             orderby s.Date descending
                             select new CandidateModel
                             {
                                 Id = c.Id,
                                 First_Name = c.First_Name + " " + c.Last_Name,
                                 Email = c.Email,
                                 Experience = c.Experience,
                                 Date = s.Date,
                                 Time_From = s.Time_From,
                                 Time_To = s.Time_To,
                             }).ToList();
            merge.CandidatesList = new List<CandidateModel>();
            merge.CandidatesList = candidatelist;         
            
            return View(merge);

        }
        //To check email exists 
        public JsonResult IsAlready(string Email)
        {
            tbl_candidates candidates = new tbl_candidates();

            using (var context = new TestEntities())
            {
                candidates = context.tbl_candidates.Where(a => a.Email.ToLower() == Email.ToLower()).FirstOrDefault();
            }


            bool status;
            if (candidates != null)
            {
                //Already registered  
                status = false;
            }
            else
            {
                //Available to use  
                status = true;
            }

            return Json(status, JsonRequestBehavior.AllowGet);

        }
        //Ajax call to auto fill
        public JsonResult ScheduleInterview1(int ? ID)
        {
             List < CandidateModel > candidatelist = new List<CandidateModel>();


            candidatelist = (from c in db.tbl_candidates
                             where c.Id == ID
                             select new CandidateModel
                             {
                                 First_Name = c.First_Name,
                                 Last_Name=c.Last_Name,
                                 Email=c.Email,
                                 Mobile=c.Mobile,
                                 Experience=c.Experience,
                             }).ToList();

            return Json(candidatelist, JsonRequestBehavior.AllowGet); 
        }
        
       
    }
}