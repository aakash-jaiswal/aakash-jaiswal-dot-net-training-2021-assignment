﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SuccessiveDotNetTest.Models
{
    public class CandidateModel
    {
        
        public int Id { get; set; }
        [Required(ErrorMessage = "Mandatory Field")]
        [RegularExpression(@"^[a-zA-Z]+[ a-zA-Z-_]*$", ErrorMessage = "Use Characters only")]
        [MaxLength(50, ErrorMessage = "Max Length allowed=50")]
        [Display(Name = "First Name")]
        public string First_Name { get; set; }
        [Required(ErrorMessage = "Mandatory Field")]
        [RegularExpression(@"^[a-zA-Z]+[ a-zA-Z-_]*$", ErrorMessage = "Use Characters only")]
        [MaxLength(50, ErrorMessage = "Max Length allowed=50")]
        [Display(Name = "Last Name")]
        public string Last_Name { get; set; }
        [Required(ErrorMessage = "Mandatory Field")]
        [Display(Name = "Date of Birth")]
        public string Dob { get; set; }
        [Required(ErrorMessage = "Mandatory Field")]
        public string Experience { get; set; }

        public string Mobile { get; set; }
        //[Required(ErrorMessage = "Mandatory Field")]
       
        //[Remote("IsAlready", "Home", ErrorMessage = "EmailId already exists in database.")]
        //[EmailAddress(ErrorMessage = "Email format is not right ")]
        public string Email { get; set; }  
        /// <summary>
        /// For View
        /// </summary>
        public string Date { get; set; }
        public string Time_From { get; set; }
        public string Time_To { get; set; }
        
    }
}