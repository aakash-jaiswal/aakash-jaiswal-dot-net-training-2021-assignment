﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuccessiveDotNetTest.Models
{
    public class InterviewModel
    {
        public int Id { get; set; }
        public int Candidate_Id { get; set; }
        [Required(ErrorMessage ="Mandatory Field")]
        [Display(Name = "Schedule Date")]
        public string Date { get; set; }
        [Required(ErrorMessage = "Mandatory Field")]
        [Display(Name = "Time From")]
        public string Time_From { get; set; }
        [Display(Name = "Time Till")]
        public string Time_To { get; set; }
        [RegularExpression(@"^[a-zA-Z]+[ a-zA-Z-_]*$", ErrorMessage = "Use Characters only")]
        [MaxLength(50,ErrorMessage ="Max Length allowed=50")]
        [Display(Name = "Interviewer Name")]
        public string Interviewer_Name { get; set; }
    }
}