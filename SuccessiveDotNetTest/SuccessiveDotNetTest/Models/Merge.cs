﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SuccessiveDotNetTest.Models
{
    public class Merge
    {
        public CandidateModel candidateModel{ get; set; }
        public List<CandidateModel> CandidatesList { get; set; }
        public InterviewModel interviewModel { get; set; }

        public SelectList CanDdl { get; set; }
        public bool IsNewCandidate { get; set; }
        [Remote("IsAlready", "Home", ErrorMessage = "EmailId already exists in database.")]
        [EmailAddress(ErrorMessage = "Email format is not right ")]
        public string Email { get; set; }
       

    }
}